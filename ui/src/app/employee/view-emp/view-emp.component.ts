import { Component, OnInit , Input} from '@angular/core';
import { UserService } from '../../_services';

@Component({
  selector: 'app-view-emp',
  templateUrl: './view-emp.component.html',
  styleUrls: ['./view-emp.component.css']
})
export class ViewEmpComponent implements OnInit {
  constructor(private service:UserService) { }

  @Input() users:any;
  UserId:string;
  firstName:string;
  lastName:string;
  usertype:string;
  username:string;
  password:string;
  usertypeList:any=[];

  UserList:any=[];

  ngOnInit(): void {
    this.loadEmployeeList();
  }
  loadEmployeeList(){
    
    this.service.getAllusertype().subscribe((data:any)=>{
      this.usertypeList=data;
    });
      this.UserId=this.users.UserId;
      this.firstName=this.users.firstName;
      this.lastName=this.users.lastName;
      this.usertype=this.users.usertype;
      this.username=this.users.username;
      this.password=this.users.password;
  }
  

}
