import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services';


@Component({
  selector: 'app-show-emp',
  templateUrl: './show-emp.component.html',
  styleUrls: ['./show-emp.component.css']
})
export class ShowEmpComponent implements OnInit {

  constructor(private service:UserService){ }

  UserList:any=[];
  UserListview:any=[];
  ModalTitle:string;
  ActivateAddEditEmpComp:boolean=false;
  ActivateviewEmpComp:boolean=false;
  users:any;

  UserIdFilter:string="";
  UserNameFilter:string="";
  UserListWithoutFilter:any=[];

  ngOnInit(): void {
    
    this.refreshUsersList();
  }

  addClick(){
    this.users={
        UserId:0,
        firstName:"",
        lastName:"",
        usertype:"",
        username:"",
        password:""
    }
    this.ModalTitle="Add User";
    this.ActivateAddEditEmpComp=true;

  }

  editClick(item){
    console.log(item);
    this.users=item;
    this.ModalTitle="Edit User";
    this.ActivateAddEditEmpComp=true;
  }

  deleteClick(item){
    if(confirm('Are you sure??')){
      this.service.deleteEmployee(item.UserId).subscribe(data=>{
        alert(data.toString());
        this.refreshUsersList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditEmpComp=false;
    this.ActivateviewEmpComp=false;
    this.refreshUsersList();
  }
  viewClick(item){
    
      this.service.viewEmployee(item.UserId).subscribe(data=>{
        this.UserListview=data;
        this.ActivateviewEmpComp=true;
        this.users=item
      })
    }
  

  refreshUsersList(){
    
    this.service.getEmpList().subscribe(data=>{
      console.log(data)
      this.UserList=data;
    });
  }
  


}

