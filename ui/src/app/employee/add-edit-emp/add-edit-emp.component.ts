import { Component, OnInit,Input } from '@angular/core';
import { UserService } from '../../_services';

@Component({
  selector: 'app-add-edit-emp',
  templateUrl: './add-edit-emp.component.html',
  styleUrls: ['./add-edit-emp.component.css']
})
export class AddEditEmpComponent implements OnInit {

  constructor(private service:UserService) { }

  @Input() users:any;
  UserId:string;
  firstName:string;
  lastName:string;
  usertype:string;
  username:string;
  password:string;
  usertypeList:any=[];

  ngOnInit(): void {
    this.loadUsertypeList();
  }

  loadUsertypeList(){
   
    this.service.getAllusertype().subscribe((data:any)=>{
      this.usertypeList=data;
    });
      this.UserId=this.users.UserId;
      this.firstName=this.users.firstName;
      this.lastName=this.users.lastName;
      this.usertype=this.users.usertype;
      this.username=this.users.username;
      this.password=this.users.password;
  }

  addEmployee(){
    var val = {UserId:this.UserId,
      firstName:this.firstName,
      lastName:this.lastName,
      username:this.username,
      usertype:this.usertype,
      password:this.password
      };

    this.service.addEmployee(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateEmployee(){
    var val = {UserId:this.UserId,
      firstName:this.firstName,
      lastName:this.lastName,
      username:this.username,
      usertype:this.usertype,
      password:this.password
      };

    this.service.updateEmployee(val).subscribe(res=>{
    alert(res.toString());
    });
  }


  


}

