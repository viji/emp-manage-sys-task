﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {
    readonly APIUrl = "http://127.0.0.1:8000";
    readonly PhotoUrl = "http://127.0.0.1:8000/media/";
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/` + id);
    }

    register(user: User) {
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }
    adduser(val:any) {
      return this.http.post(this.APIUrl + '/user/',val);
  }

    update(user: User) {
        return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/` + id);
    }
    getskillsList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/skills/');
  }
  getuserskill(val:any){
    return this.http.get(this.APIUrl + '/userskill/'+val);
  }

  addskills(val:any){
    return this.http.post(this.APIUrl + '/skills/',val);
  }

  updateskills(val:any){
    return this.http.put(this.APIUrl + '/skills/',val);
  }

  deleteskills(val:any){
    return this.http.delete(this.APIUrl + '/skills/'+val);
  }


  getEmpList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/employee/');
  }

  addEmployee(val:any){
    return this.http.post(this.APIUrl + '/employee/',val);
  }

  updateEmployee(val:any){
    return this.http.put(this.APIUrl + '/employee/',val);
  }

  deleteEmployee(val:any){
    return this.http.delete(this.APIUrl + '/employee/'+val);
  }
  viewEmployee(val:any){
    return this.http.get(this.APIUrl + '/employee/'+val);
  }

  UploadPhoto(val:any){
    return this.http.post(this.APIUrl+'/SaveFile',val);
  }

  getAllskillsNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/skills/');
  }
  
  getAllEmployeeNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/employeenames/');
  }
  getAllusertype():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/usertype/');
  }
  
}