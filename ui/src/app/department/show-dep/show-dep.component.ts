import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services';
import { Router } from '@angular/router';
import { User } from '../../_models';

@Component({
  selector: 'app-show-dep',
  templateUrl: './show-dep.component.html',
  styleUrls: ['./show-dep.component.css']
})
export class ShowDepComponent implements OnInit {
  currentUser: User;
  constructor(private service:UserService,private router : Router) { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  skillList:any=[];
  url:any;
  ModalTitle:string;
  ActivateAddEditDepComp:boolean=false;
  skill:any;

  skillIdFilter:string="";
  skillNameFilter:string="";
  skillListWithoutFilter:any=[];
  activepercentage:boolean;

  ngOnInit(): void {
    this.url=this.router.url;
    if(this.currentUser.usertype=="Employee"){
      this.activepercentage=false;
    }
    else{
      this.activepercentage=true;
    }
    this.refreshskillList();
  }

  addClick(){
    if(this.url="/userskill"){
      this.skill={
        SkillId:0,
        SkillName:"",
        employee:this.currentUser.username,
        percentage:""
      }
    }
    else{
    this.skill={
      SkillId:0,
      SkillName:"",
      employee:"",
      percentage:0
    }
  }
    this.ModalTitle="Add Skill";
    this.ActivateAddEditDepComp=true;

  }

  editClick(item){
    this.skill=item;
    this.ModalTitle="Edit Skill";
    this.ActivateAddEditDepComp=true;
  }

  deleteClick(item){
    if(confirm('Are you sure??')){
      this.service.deleteskills(item.SkillId).subscribe(data=>{
        alert(data.toString());
        this.refreshskillList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditDepComp=false;
    this.refreshskillList();
  }


  refreshskillList(){
    
    
    if(this.url="/userskill"){
      this.service.getuserskill(this.currentUser.username).subscribe(data=>{
        console.log(data)
        this.skillList=data;
        this.skillListWithoutFilter=data;
      });
    }
    else{
    this.service.getskillsList().subscribe(data=>{
      this.skillList=data;
      this.skillListWithoutFilter=data;
    });
  }
  }

  FilterFn(){
    var skillIdFilter = this.skillIdFilter;
    var skillNameFilter = this.skillNameFilter;

    this.skillList = this.skillListWithoutFilter.filter(function (el){
        return el.SkillId.toString().toLowerCase().includes(
          skillIdFilter.toString().trim().toLowerCase()
        )&&
        el.SkillName.toString().toLowerCase().includes(
          skillNameFilter.toString().trim().toLowerCase()
        )
    });
  }

  sortResult(prop,asc){
    this.skillList = this.skillListWithoutFilter.sort(function(a,b){
      if(asc){
          return (a[prop]>b[prop])?1 : ((a[prop]<b[prop]) ?-1 :0);
      }else{
        return (b[prop]>a[prop])?1 : ((b[prop]<a[prop]) ?-1 :0);
      }
    })
  }

}
