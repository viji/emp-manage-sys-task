import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Component, OnInit,Input } from '@angular/core';
import { UserService } from '../../_services';
import { AppComponent }  from '../../app.component';
import { Router } from '@angular/router';
import { User } from '../../_models';

@NgModule({
    imports:      [ BrowserModule, FormsModule ],
    declarations: [ AppComponent ],
    bootstrap:    [ AppComponent ]
})
@Component({
  selector: 'app-add-edit-dep',
  templateUrl: './add-edit-dep.component.html',
  styleUrls: ['./add-edit-dep.component.css']
})
export class AddEditDepComponent implements OnInit {

  currentUser: User;
  constructor(private service:UserService,private router : Router) { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  @Input() skill:any;
  SkillId:string;
  SkillName:string;
  employee:string;
  percentage:string;
  EmployeeList:any
  url:any;
  activeemp:boolean

  ngOnInit(): void {
    this.url=this.router.url;
    if(this.currentUser.usertype=="Employee"){
      this.activeemp=false;
    }
    else{
      this.activeemp=true;
    }
    this.loadEmployeeList();
  }
  loadEmployeeList(){
    this.service.getAllEmployeeNames().subscribe((data:any)=>{
      console.log(data)
        this.EmployeeList=data;
        
      
    });
    this.SkillId=this.skill.SkillId;
    this.SkillName=this.skill.SkillName;
    this.employee=this.skill.employee;
    this.percentage=this.skill.percentage;
  }

  addskill(){
    if(this.url="/userskill"){
      var val = {SkillId:this.SkillId,
        SkillName:this.SkillName,
        employee:this.currentUser.username,
        percentage:this.percentage};
    }
    else{
      var val = {SkillId:this.SkillId,
        SkillName:this.SkillName,
        employee:this.employee,
        percentage:this.percentage};
  }
    
    this.service.addskills(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateskill(){
    if(this.url="/userskill"){
    var val = {SkillId:this.SkillId,
      SkillName:this.SkillName,
      employee:this.currentUser.username,
      percentage:this.percentage};
  }
  else{
    var val = {SkillId:this.SkillId,
      SkillName:this.SkillName,
      employee:this.employee,
      percentage:this.percentage};
}
    this.service.updateskills(val).subscribe(res=>{
    alert(res.toString());
    });
  }

}
