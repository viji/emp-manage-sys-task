from django.db import models

# Create your models here.

class Skills(models.Model):
    SkillId = models.AutoField(primary_key=True)
    SkillName = models.CharField(max_length=100)
    employee= models.CharField(max_length=100)
    percentage= models.CharField(max_length=100)
    class Meta:
        db_table="emp_Skills"


class Users(models.Model):
    UserId = models.AutoField(primary_key=True)
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    usertype = models.CharField(max_length=100)
    username= models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    class Meta:
        db_table="emp_users"


class Usertype(models.Model):
    Id = models.AutoField(primary_key=True)
    account_name = models.CharField(max_length=100)
    class Meta:
        db_table="emp_usertype"