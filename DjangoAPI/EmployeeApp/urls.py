from django.conf.urls import url
from EmployeeApp import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns=[
    url(r'^skills/$',views.skillApi),
    url(r'^skills/([0-9]+)$',views.skillApi),
    url(r'^userskill/([a-zA-Z]+)$',views.userskill),

    url(r'^employee/$',views.employeeApi),
    url(r'^employee/([0-9]+)$',views.employeeApi),
    url(r'^employeenames/$',views.getemployee),
    url(r'^login/$',views.emplogin),
    url(r'^usertype/$',views.usertypeApi),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)