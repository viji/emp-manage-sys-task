from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from django.contrib.auth import authenticate,get_user_model,login,logout
from EmployeeApp.models import Skills,Users,Usertype
from EmployeeApp.serializers import SkillSerializer,UsersSerializer,UsertypeSerializer
from django.conf import settings
from django.core.files.storage import default_storage
import os
import base64

# Create your views here.
@csrf_exempt
def skillApi(request,id=0):
    if request.method=='GET':
        skills = Skills.objects.all()
        skills_serializer = SkillSerializer(skills, many=True)
        return JsonResponse(skills_serializer.data, safe=False)

    elif request.method=='POST':
        skills_data=JSONParser().parse(request)
        print(skills_data)
        skills_serializer = SkillSerializer(data=skills_data)
        if skills_serializer.is_valid():
            skills_serializer.save()
            return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    
    elif request.method=='PUT':
        skills_data = JSONParser().parse(request)
        print(skills_data)
        skills=Skills.objects.get(SkillId=skills_data['SkillId'])
        skills_serializer=SkillSerializer(skills,data=skills_data)
        if skills_serializer.is_valid():
            skills_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method=='DELETE':
        skills=Skills.objects.get(SkillId=id)
        skills.delete()
        return JsonResponse("Deleted Succeffully!!", safe=False)

@csrf_exempt
def employeeApi(request,id=0):
    if request.method=='GET':
        if id==0:
            users = Users.objects.all()
            users_serializer = UsersSerializer(users, many=True)
            return JsonResponse(users_serializer.data, safe=False)
        else:
            users = Users.objects.get(UserId=id)
            users_serializer = UsersSerializer(users)
            return JsonResponse(users_serializer.data, safe=False)


    elif request.method=='POST':
        Users_data=JSONParser().parse(request)
        Users_serializer = UsersSerializer(data=Users_data)
        if Users.objects.filter(username=Users_data['username']).exists():
            return JsonResponse("username already exists!" , safe=False)
        else:
            if Users_serializer.is_valid():
                Users_serializer.save()
                return JsonResponse("Added Successfully!!" , safe=False)
        return JsonResponse("Failed to Add.",safe=False)
    
    elif request.method=='PUT':
        Users_data = JSONParser().parse(request)
        print(Users_data['UserId'])
        #print(Users.objects.all().exclude(UserId=2))
        print(Users.objects.filter(username=Users_data['username']).exclude(UserId=Users_data['UserId']))
        if  Users.objects.filter(username=Users_data['username']).exclude(UserId=Users_data['UserId']):
            return JsonResponse("username already exists!" , safe=False)
        else:
            users=Users.objects.get(UserId=Users_data['UserId'])
            Users_serializer=UsersSerializer(users,data=Users_data)
            if Users_serializer.is_valid():
                Users_serializer.save()
                return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method=='DELETE':
        users=Users.objects.get(UserId=id)
        users.delete()
        return JsonResponse("Deleted Succeffully!!", safe=False)




@csrf_exempt
def emplogin(request,id=0):
    if request.method=='POST':
        user_values={}
        message={}
        employee_data=JSONParser().parse(request)
        #print(employee_data)
        if employee_data!={}:
            username=employee_data['username']
            password=employee_data['password']

            if employee_data!="":
                #user_info = EmployeeUsers.objects.filter(username=username,password=password)
                ##print(user_info.query)
                try:
                    user_info = Users.objects.get(username=username,password=password)
                except:
                    user_info = ""
                print(user_info)
                ##print(type(user_info.status))
                if user_info!="":
                    user_serializer = UsersSerializer(user_info, many=True)
                    user_values['id']=user_info.UserId
                    user_values['username']=user_info.username
                    user_values['firstName']=user_info.firstName
                    user_values['lastName']=user_info.lastName
                    user_values['token']="fake-jwt-token"
                    user_values['usertype']=user_info.usertype
                    return JsonResponse(user_values,safe=False)
                    # return HttpResponseRedirect("/home")
                else:
                    #print(message)
                    message['invalid_credential']="Invalid Credentials!."
                    return JsonResponse(message,safe=False)
            else:
                #print(message)
                message['invalid_entries']="Invalid Entries!."
                return JsonResponse(message,safe=False)
                
        else:
            message['login_credential']="Please enter login credential!."
            return JsonResponse(message,safe=False)
		# #print(message)
        message['Invalid_Login']="Invalid Login!."    
        return JsonResponse(message,safe=False)





    

@csrf_exempt
def usertypeApi(request):
    if request.method=='GET':
        usertypevalues = Usertype.objects.all()
        usertype_serializer = UsertypeSerializer(usertypevalues, many=True)
        return JsonResponse(usertype_serializer.data, safe=False)

@csrf_exempt
def userskill(request,val=""):
    if request.method=='GET':
        skills = Skills.objects.filter(employee=val)
        skills_serializer = SkillSerializer(skills, many=True)
        return JsonResponse(skills_serializer.data, safe=False)
@csrf_exempt
def getemployee(request):
    if request.method=='GET':
        users = Users.objects.filter(usertype="Employee")
        print(users)
        users_serializer = UsersSerializer(users,many=True)
        return JsonResponse(users_serializer.data, safe=False)