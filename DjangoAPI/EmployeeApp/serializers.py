from rest_framework import serializers
from EmployeeApp.models import Skills, Users,Usertype



class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skills
        fields = ('SkillId',
                  'SkillName',
                  'employee',
                  'percentage')



class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('UserId',
                  'firstName',
                  'lastName',
                  'usertype',
                  'username',
                  'password')

class UsertypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usertype
        fields = ('Id',
                  'account_name'
                  )